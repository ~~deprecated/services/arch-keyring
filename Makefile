V=20200624

PREFIX = /usr/local

install:
	install -dm755 $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
	install -dm755 $(DESTDIR)$(PREFIX)/share/licenses/arch-keyring/
	install -m0644 arch{.gpg,-trusted,-revoked} $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
	install -m0644 legalcode.txt $(DESTDIR)$(PREFIX)/share/licenses/arch-keyring/

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/share/pacman/keyrings/arch{.gpg,-trusted,-revoked}
	rm -f $(DESTDIR)$(PREFIX)/share/licenses/arch-keyring/legalcode.txt
	rmdir -p --ignore-fail-on-non-empty $(DESTDIR)$(PREFIX)/share/pacman/keyrings/
	rmdir -p --ignore-fail-on-non-empty $(DESTDIR)$(PREFIX)/share/licenses/arch-keyring/

dist:
	mkdir -pv arch-keyring-$(V)/
	cp -v Makefile arch.gpg arch-revoked arch-trusted legalcode.txt arch-keyring-$(V)/
	bsdtar czf arch-keyring-$(V).tar.gz arch-keyring-$(V)/
	gpg --detach-sign --use-agent arch-keyring-$(V).tar.gz
	rm -rv arch-keyring-$(V)/

upload:
	rsync -av -e 'ssh -p 51000' --progress arch-keyring-$(V).tar.gz{,.sig} repo@dusseldorf.hyperbola.info:/srv/repo/sources/arch-keyring

.PHONY: install uninstall dist upload
